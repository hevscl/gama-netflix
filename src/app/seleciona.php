<?php
    include 'verificacao.php';
?>

<script language = javascript type = "text/javascript">          
    function videos(){
        window.location.assign ("espacoOnline.php");
    }
    function avaliar(){
        window.location.assign ("avaliacao.php");
    }
</script>

<!DOCTYPE HTML>
<html lang="pt-br">
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous"> 
    
    <title>Seleciona</title>
    
	<head>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">        
        <link rel = "stylesheet" href = "seleciona.css">
	</head>	
	<body>
        <div class="row row_principal">
            <div class="col">
                <div class ="row">
                    <img class="imagem" src="./imagens/casal-login.jpg" alt = "um homem e uma mulher sentados em frente ao notebook e rindo">
                </div> 
            </div>
            
            <div class="col card text-center">
                    <img style="height: 4vw" src = "imagens/logo.svg" alt = "jobs4all">

                    <div class = "form-group">
                        <h1>Seja bem vindx</h1>
                    </div>

                    <div class = "caixa">
                        <input type = "button" class= "btn btn-primary" value = "Assistir aos vídeos" onClick = "videos();" style = "margin-top: 2rem;"/><br>
                        <input type = "button" class= "btn btn-primary" value = "Avaliar o evento" onClick = "avaliar();"/>
                    </div>                               
                                
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col" style="height: 10px; background-color: #003CFF"></div>
            <div class="col" style="height: 10px; background-color: #DD0830"></div>
            <div class="col" style="height: 10px; background-color: #FF9D00"></div>
            <div class="col" style="height: 10px; background-color: #0ACF2E"></div>
        </div>
        
        <script src= "https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity= "sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin= "anonymous" ></script>
        
	</body>
</html>