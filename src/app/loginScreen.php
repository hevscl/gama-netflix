<?php
    session_start();
?>

<!DOCTYPE html>
<html lang = "pt-br">
    <head>        
        <meta charset = "utf-8">
        <meta name = "viewport" content = "width=device-width, initial-scale=1, shrink-to-fit=no">

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
        integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
        integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" 
        integrity = "sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin = "anonymous">
        <link rel = "stylesheet" href = "login.css">
        <link href='https://fonts.googleapis.com/css?family=Poppins' rel='stylesheet'>
        
        <title>Jobs4All - Login</title>
        <link rel="icon" type="image/svg" href="./imagens/Logo2.svg">
    </head>

    <body> 
        <div class="row row_principal">
            <div class="col">
                <div class ="row">
                    <img class="imagem" src="./imagens/casal-login.jpg" alt = "um homem e uma mulher sentados em frente ao notebook e rindo">
                </div> 
            </div>
            
            <div class="col log">
                    <img style="height: 50px" src = "imagens/logo.svg" alt = "jobs4all">

                    <form method = "POST" action = "tratamentologin.php">
                        <div class = "form-group">
                            <input type = "text" name = "ingresso" placeholder = "Código do ingresso" class = "form-control" required = "required">
                            <span class = "linha"></span>
                        </div>

                        <div class = "erro">
                            <?php
                                if (isset ($_SESSION['msg'])){
                                    echo $_SESSION['msg'];
                                    unset ($_SESSION['msg']);
                                }	
                            ?>  
                        </div>

                        <div>
                            <button class = "btn botao-entrar" type = "submit">ENTRAR</button>
                        </div>                    
                    </form>            
                                
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col" style="height: 10px; background-color: #003CFF"></div>
            <div class="col" style="height: 10px; background-color: #DD0830"></div>
            <div class="col" style="height: 10px; background-color: #FF9D00"></div>
            <div class="col" style="height: 10px; background-color: #0ACF2E"></div>
        </div>
    </body>
</html>