<?php
    include 'verificacao.php';

    $servidor = "34.234.72.204";
    $usuario = "jobs4all";
    $senha = "jobs4all";
    $dbname = "jobs4all";

    $conn = mysqli_connect($servidor,  $usuario, $senha, $dbname); //conexao com o bd

    $ingresso = $_SESSION['ingresso'];
    $teste = "SELECT * FROM presenca WHERE ingresso = '$ingresso';"; //verifica se a pessoa ja votou
    
    $verifica = mysqli_query($conn, $teste);
    if (mysqli_num_rows($verifica) == 0){//Se nao tiver
        $insert = "INSERT INTO presenca(ingresso, modalidade) VALUES ('$ingresso', (SELECT modalidade FROM login WHERE ingresso = '$ingresso'));";
       
        $resultado = mysqli_query($conn, $insert);
    }
?>

<!DOCTYPE html>
<html lang="pt-br">

    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content = "width=device-width, initial-scale=1.0" />
          
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" 
        integrity = "sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin = "anonymous">
        <link href='https://fonts.googleapis.com/css?family=Poppins' rel='stylesheet'>  <!-- GOOGLE FONTS-->
        <link rel="stylesheet" href="espacoOnline.css">

        <script type="text/javascript" src="http://code.jquery.com/jquery-1.7.2.min.js"></script>

        <title>Vídeos</title>
        <link rel="icon" type="image/svg" href="imagens/Logo2.svg">
    </head>

    <body>        
        <nav class="navbar navbar-expand-lg navbar-light">
            <a class="navbar-brand" href="index.html"><img style = "height: 3.2vw;"  src = "imagens/logo.svg" alt = "jobs4all"></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>   
            
            <div class="collapse navbar-collapse" id="navbarSupportedContent" style = "margin-left: 61%">
                <ul class="navbar-nav" style = "font-size: 1.5vw;">
                    <li class="nav-item">
                        <a class="nav-link" style = "font-family: Poppins;" href="index.html">Sair</a>
                    </li>
                </ul>
            </div>
        </nav>           
        
        <div class = "titulos">
            <div id = "header">
                <ul class="abas">
                    <li>
                        <div class="aba">
                            <span>Design e acessibilidade</span>
                        </div>
                    </li>
                    <li>
                        <div class="aba">
                            <span>Branding inclusivo</span>
                        </div>
                    </li>
                    <li>
                        <div class="aba">
                            <span>Mesa redonda</span>
                        </div>
                    </li>
                </ul>
            </div>
            <div id = "content">
                <div class = "conteudo">
                <iframe class = "videos"  src="https://www.youtube.com/embed/xNzKLLJBNKI" 
                    frameborder = "0" allow = "accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" 
                    allowfullscreen></iframe>
                    <h1>Design e acessibilidade: Projetando para todos os públicos</h1>
                </div>
                <div class = "conteudo">
                    <iframe class = "videos" src = "https://www.youtube.com/embed/HuimA2WC8tw" frameborder = "0" 
                        allow = "accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" 
                        allowfullscreen></iframe>
                    <h1>Branding inclusivo: como empresas podem dialogar com a diversidade</h1>
                </div>
                <div class = "conteudo">
                    <iframe class = "videos" src = "https://www.youtube.com/embed/JWwuwyKaJzI" frameborder = "0" 
                        allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" 
                        allowfullscreen></iframe>
                    <h1>Mesa redonda: Diversidade nas empresas de Tecnologia</h1>
                </div>
            </div>
        </div>

        <script type="text/javascript" src="espacoOnline.js"></script>
    </body>
</html>