const express = require('express');
const fs = require('fs');
const http = require('http');
const path = require('path');

const app = express();
const port = process.env.PORT || 8080


var phpExpress = require('php-express')({
    binPath: '/usr/bin/php'
  });
  
// set view engine to php-express
app.set('views', __dirname + '/app');
app.engine('php', phpExpress.engine);
app.set('view engine', 'php');

// routing all .php file to php-express
app.all(/.+\.php$/, phpExpress.router);

app.use(express.static(__dirname + '/app'));

app.get('/*', (req, res) => res.sendFile(path.join(__dirname)));

const server = http.createServer(app);

server.listen(port,() => console.log('Funcionando'))